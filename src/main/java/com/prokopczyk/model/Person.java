package com.prokopczyk.model;

/**
 * Created by andrzej on 2015-03-05.
 */
public class Person {
    private String name;
    private String surname;
    private int age;
    private double weight;
    private char sex;

    public Person(String name, String surname, int age, double weight, char sex) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.weight = weight;
        this.sex = sex;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public double getWeight() {
        return weight;
    }

    public char getSex() {
        return sex;
    }
}
